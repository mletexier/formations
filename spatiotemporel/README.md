# Exploration spatio-temporelle d’objets géographiques ponctuels

Fiche RZine portant sur l'analyse de données spatio-temporelles ponctuelles, à partir de l'exemple de la base de données MERIMEE qui consigne l’ensemble des bâtiments classés aux monuments historiques.

-> [Consulter le document](https://gitlab.huma-num.fr/mletexier/formations/spatiotemporel/)
